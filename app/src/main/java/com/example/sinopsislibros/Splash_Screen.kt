package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class Splash_Screen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash__screen)
        Thread.sleep(2000)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}