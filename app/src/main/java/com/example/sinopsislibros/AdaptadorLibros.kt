package com.example.sinopsislibros

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdaptadorLibros (var context: Context, items: ArrayList<Libros_Card>, var listener: ClickListener): RecyclerView.Adapter<AdaptadorLibros.ViewHolder>() {

    var items:ArrayList<Libros_Card>? = null
    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorLibros.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.card_libro,parent,false)
        val viewHolder = AdaptadorLibros.ViewHolder(vista, listener)

       return viewHolder

    }

    override fun getItemCount(): Int {

        return items?.count()!!

    }

    override fun onBindViewHolder(holder: AdaptadorLibros.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.foto?.setImageResource(item?.imagen!!)
        holder.nombre?.text = item?.nombre
        holder.descripcion?.text = item?.descripcion

    }

    class ViewHolder(vista: View, listener: ClickListener):RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var foto: ImageView? = null
        var nombre: TextView? = null
        var descripcion: TextView? = null
        var listener:ClickListener? = null

        init {
            foto = vista.findViewById(R.id.imagen)
            nombre = vista.findViewById(R.id.nombre)
            descripcion = vista.findViewById(R.id.descripcion)

            this.listener = listener
            vista.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }

    }

}