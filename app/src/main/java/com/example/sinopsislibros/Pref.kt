package com.example.sinopsislibros

import android.content.Context

class Pref(val context: Context) {

    val SHARED_NAME = "Mydb"
    val user = "username"
    val con = "contraseña"
    val storage = context.getSharedPreferences(SHARED_NAME, 0)

    fun saveName(name:String){
        storage.edit().putString(user, name).apply()

    }

    fun saveContra(contras:String){
        storage.edit().putString(con, contras).apply()

    }

    fun getContra():String{
       return storage.getString(con, "")!!

    }

    fun getName():String {
       return storage.getString(user, "")!!
    }



}