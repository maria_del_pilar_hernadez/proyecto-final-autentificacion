package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sinopsislibros.SinopsisLibros.Companion.pref
import kotlinx.android.synthetic.main.activity_main.*

import android.app.Activity
import android.app.AlertDialog
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ScrollView
import com.airbnb.lottie.LottieAnimationView
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


import kotlinx.android.synthetic.main.activity_registro.*
import android.util.Log.d as d1


class MainActivity : AppCompatActivity() {

    private val GOOGLE_SIGN_IN = 100
    private val callbackManager = CallbackManager.Factory.create()

    companion object{
        private const val RC_SIGN_IN = 423
    }

    private lateinit var usua:String
    private lateinit var contrase:String

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        //googleLogin()

        datos.setOnClickListener {

            val intent = Intent(this,Registro::class.java)
            startActivity(intent)

        }
        iniciar.setOnClickListener(){
            ingresar()
        }

        google.setOnClickListener(View.OnClickListener {
            val googleConf: GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()

            val googleClient : GoogleSignInClient = GoogleSignIn.getClient(this, googleConf)
            googleClient.signOut()

            startActivityForResult(googleClient.signInIntent, GOOGLE_SIGN_IN)

        })


      faceboook.setOnClickListener {
         Log.d("tag","*********************************************************************************+")
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("email"))


            LoginManager.getInstance().registerCallback(callbackManager,
                object: FacebookCallback<LoginResult>{

                    override fun onSuccess(result: LoginResult?) {

                        result?.let {
                            val token = it.accessToken

                            val credential = FacebookAuthProvider.getCredential(token.token)

                            FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener{

                                if (it.isSuccessful){

                                    val intent = Intent(applicationContext, Generos::class.java)
                                    startActivity(intent)
                                }else{
                                    print("error")
                                }
                            }
                        }
                    }
                    override fun onCancel() {
                        print("error")
                    }

                    override fun onError(error: FacebookException?) {
                        print("error")
                    }
                })
        }
    }

    private fun ingresar(){
        if (usuario.text.isNotEmpty() && contra.text.isNotEmpty()){
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(usuario.text.toString(),
                    contra.text.toString()).addOnCompleteListener(this){
                    if (it.isSuccessful){
                        val intent = Intent(applicationContext, Generos::class.java)
                        startActivity(intent)
                    }else{
                        print(usuario.text.toString() + " - " + contra.text.toString())
                        Toast.makeText(applicationContext, "Datos Incorrectos", Toast.LENGTH_LONG).show()
                    }
                }
        }
    }

    private fun accion(){
        val i: Intent = Intent(this, Generos::class.java)
        i.putExtra("AudiosListos","true")
        startActivity(i)
    }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {


        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == GOOGLE_SIGN_IN){
            val task : Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account : GoogleSignInAccount? = task.getResult(ApiException::class.java)

                if(account!=null){
                    val credential : AuthCredential = GoogleAuthProvider.getCredential(account.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener{
                        if (it.isSuccessful){
                            val intent = Intent(applicationContext, Generos::class.java)
                            startActivity(intent)
                        }else{
                            Toast.makeText(applicationContext, "Datos Incorrectos", Toast.LENGTH_LONG).show()
                        }
                    }
                }

            }catch (e: ApiException){
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Error")
                builder.setMessage("Datos Incorrectos")
                builder.setPositiveButton("Aceptar", null)
                val dialog: AlertDialog = builder.create()
                dialog.show()

            }

        }
    }

}