package com.example.sinopsislibros

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdaptadorCustom(var context:Context, items: ArrayList<ListaCategoria>, var listener: ClickListener): RecyclerView.Adapter<AdaptadorCustom.ViewHolder>() {

    var items:ArrayList<ListaCategoria>? = null
    init {
        this.items = items
    }

    // el archivo xml en viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorCustom.ViewHolder {
       val vista = LayoutInflater.from(context).inflate(R.layout.categoria,parent,false)
        val viewHolder = ViewHolder(vista,listener)

        return viewHolder
    }

    override fun onBindViewHolder(holder: AdaptadorCustom.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.foto?.setImageResource(item?.foto!!)
        holder.nombre?.text = item?.titulo

    }

    override fun getItemCount(): Int {
       return items?.count()!!
    }

    class ViewHolder(vista: View, listener: ClickListener):RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var foto:ImageView? = null
        var nombre:TextView? = null
        var listener:ClickListener? = null

        init {
            foto = vista.findViewById(R.id.icono)
            nombre = vista.findViewById(R.id.titulo)
            this.listener = listener
            vista.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }

    }
}