package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Libro_fantasia : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro_fantasia)
        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("El señor de los anillos", R.drawable.pelicula, getString(R.string.anillos)))
        LibrosT.add(Libros_Card("Harry Potter", R.drawable.harry, getString(R.string.harry_potter)))
        LibrosT.add(Libros_Card("Percy Jackson", R.drawable.percy, getString(R.string.percy)))
        LibrosT.add(Libros_Card("Juego de Tronos", R.drawable.juegodetronos, getString(R.string.juego_tronos)))

        lista = findViewById(R.id.fantasia)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG)
                    .show()

                if (index == 0) {

                    val nombres = "El señor de los Anillos"
                    val autor = "Autor: J.R.R Tolkien"
                    val sinopsis = getString(R.string.senior_desc)
                    val foto = R.drawable.pelicula
                    val imagen = R.drawable.pelicula_dos
                    val cali = 3.2F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }

                if (index == 1) {

                    val nombres = "Harry Potter"
                    val autor = "Autor: J.K Rowling"
                    val sinopsis = getString(R.string.harry_desc)
                    val foto = R.drawable.harry
                    val imagen = R.drawable.harry_dos
                    val cali = 3.7F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "Percy Jackson"
                    val autor = "Autor: Rick Jordan"
                    val sinopsis = getString(R.string.percy_desc)
                    val foto = R.drawable.percy
                    val imagen = R.drawable.percy_dos
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "Juego de Tronos"
                    val autor = "Autor: George J.J Martín"
                    val sinopsis = getString(R.string.tronos_desc)
                    val foto = R.drawable.juegodetronos
                    val imagen = R.drawable.juegodetronos_dos
                    val cali = 2.5F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

            }
        })

        lista?.adapter = adaptador

    }
}
