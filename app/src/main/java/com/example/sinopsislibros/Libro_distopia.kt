package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Libro_distopia : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro_distopia)
        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("Divergente", R.drawable.diver, getString(R.string.divergente)))
        LibrosT.add(Libros_Card("Los juegos del Hambre", R.drawable.juegoshambre, getString(R.string.juego_hambre)))
        LibrosT.add(Libros_Card("Maze Runner", R.drawable.maze, getString(R.string.maze_runner)))
        LibrosT.add(Libros_Card("Cinder", R.drawable.cinder, getString(R.string.cinder)))

        lista = findViewById(R.id.disto)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG)
                    .show()


                if (index == 0) {

                    val nombres = "Divergente"
                    val autor = "Autor: Verónica Roth"
                    val sinopsis = getString(R.string.diver_desc)
                    val foto = R.drawable.diver
                    val imagen = R.drawable.diver_dos
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }

                if (index == 1) {

                    val nombres = "Los juegos del Hambre"
                    val autor = "Autor: Suzzane Collins"
                    val sinopsis = getString(R.string.juegos_desc)
                    val foto = R.drawable.juegoshambre
                    val imagen = R.drawable.pajaritojuegos
                    val cali = 2.3F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "Maze Runner"
                    val autor = "Autor: Jame Dashner"
                    val sinopsis = getString(R.string.maze_desc)
                    val foto = R.drawable.maze
                    val imagen = R.drawable.mazeee
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "Cinder"
                    val autor = "Autor: Marissa Meyer"
                    val sinopsis = getString(R.string.cinder_desc)
                    val foto = R.drawable.cinder
                    val imagen = R.drawable.cin
                    val cali = 3.7F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

            }
        })

        lista?.adapter = adaptador

    }
}
