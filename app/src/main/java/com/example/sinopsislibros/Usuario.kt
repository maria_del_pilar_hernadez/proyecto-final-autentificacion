package com.example.sinopsislibros

class Usuario {

    var nombre: String = ""
        get() = field
        set(value) {
            field = value
        }

    var apellidos: String = ""
        get() = field
        set(value) {
            field = value
        }

    var correo: String = ""
        get() = field
        set(value) {
            field = value
        }

    var clave: String = ""
        get() = field
        set(value) {
            field = value
        }

    constructor() {

    }

    constructor(nombre: String, apellidos: String, correo: String, clave: String) {
        this.nombre = nombre
        this.apellidos = apellidos
        this.correo = correo
        this.clave = clave
    }
}