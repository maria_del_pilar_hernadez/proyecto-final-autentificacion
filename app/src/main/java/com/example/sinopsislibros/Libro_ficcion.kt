package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Libro_ficcion : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro_ficcion)

        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("Marciano", R.drawable.marciano, getString(R.string.marciano)))
        LibrosT.add(Libros_Card("Yo, Robot", R.drawable.yorobot, getString(R.string.robot)))
        LibrosT.add(Libros_Card("1984", R.drawable.anio, getString(R.string.libro)))
        LibrosT.add(Libros_Card("La guerra de los mundos", R.drawable.guerramundos, getString(R.string.guerra)))

        lista = findViewById(R.id.ficcion)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG)
                    .show()

                if (index == 0) {

                    val nombres = "El Marciano"
                    val autor = "Autor: Andy Weir"
                    val sinopsis = getString(R.string.marciano_desc)
                    val foto = R.drawable.marciano
                    val imagen = R.drawable.marciano_dos
                    val cali = 1.2F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }

                if (index == 1) {

                    val nombres = "Yo, Robot"
                    val autor = "Autor: Isaac Asimov"
                    val sinopsis = getString(R.string.robot_desc)
                    val foto = R.drawable.yorobot
                    val imagen = R.drawable.yorobot_dos
                    val cali = 2.7F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "1984"
                    val autor = "Autor: George Orwell"
                    val sinopsis = getString(R.string.anio_desc)
                    val foto = R.drawable.anio
                    val imagen = R.drawable.anios_dos_l
                    val cali = 3.6F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "La Guerra de los Mundos"
                    val autor = "Autor: Herbet George"
                    val sinopsis = getString(R.string.guerra_desc)
                    val foto = R.drawable.guerramundos
                    val imagen = R.drawable.guerramundos_dos
                    val cali = 3.2F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

            }
        })

        lista?.adapter = adaptador

    }
}
