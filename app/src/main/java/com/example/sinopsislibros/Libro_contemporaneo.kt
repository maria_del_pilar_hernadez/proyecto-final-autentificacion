package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Libro_contemporaneo : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro_contemporaneo)

        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("Persona Normal", R.drawable.personanormal, getString(R.string.persona_normal)))
        LibrosT.add(Libros_Card("Buscando Alaska", R.drawable.buscando, getString(R.string.alaska)))
        LibrosT.add(Libros_Card("La sombra del viento", R.drawable.sombra, getString(R.string.sombra)))
        LibrosT.add(Libros_Card("Las ventajas de ser invisible", R.drawable.ventajas, getString(R.string.invisible)))

        lista = findViewById(R.id.contem)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG)
                    .show()

                if (index == 0) {

                    val nombres = "Persona Normal"
                    val autor = "Autor: Benito Taibo"
                    val sinopsis = getString(R.string.persona_desc)
                    val foto = R.drawable.personanormal
                    val imagen = R.drawable.personanormal_dos
                    val cali = 1.5F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }

                if (index == 1) {

                    val nombres = "Buscando Alaska"
                    val autor = "Autor: John Green"
                    val sinopsis = getString(R.string.alaska_desc)
                    val foto = R.drawable.buscando
                    val imagen = R.drawable.buscandoalaska
                    val cali = 3.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "La Sombra del Viento"
                    val autor = "Autor: Carlos Ruíz Z."
                    val sinopsis = getString(R.string.sombra_desc)
                    val foto = R.drawable.sombra
                    val imagen = R.drawable.sombra_dos
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "Las Ventajas de ser Invisible"
                    val autor = "Autor: Stephen Chbosky"
                    val sinopsis = getString(R.string.invisible_desc)
                    val foto = R.drawable.ventajas
                    val imagen = R.drawable.invisible_dos
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }


            }
        })

        lista?.adapter = adaptador

    }
}
