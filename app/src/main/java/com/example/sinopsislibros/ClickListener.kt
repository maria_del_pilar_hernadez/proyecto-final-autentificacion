package com.example.sinopsislibros

import android.view.View

interface ClickListener {
    fun onClick(vista: View, index:Int)
}