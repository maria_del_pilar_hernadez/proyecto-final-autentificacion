package com.example.sinopsislibros

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_generos.view.*
import kotlinx.android.synthetic.main.activity_informacion.*

class Informacion : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_informacion)

        val bundle : Bundle? = intent.extras

        bundle?.let { bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombre", "Desconocido")
            val escritor = bundleLibriDeNull.getString("key_autor", "Desconocido")
            val desc = bundleLibriDeNull.getString("key_sinopsis", "Desconocido")
            val fot = bundleLibriDeNull.getInt("key_imagen", 0)
            val image = bundleLibriDeNull.getInt("key_img", 0)
            val estrellas = bundleLibriDeNull.getFloat("key_cal", 0.0f)

            titulo.text = "$nombres"
            autor.text = "$escritor"
            texto.text = "$desc"
            portada.setImageResource(fot)
            imagen.setImageResource(image)
            calificacion.rating = estrellas

        }
        }
    }
