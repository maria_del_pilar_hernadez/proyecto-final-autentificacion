package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Libro_Clasicos : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro__clasicos)

        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("100 años de soledad", R.drawable.soledad, getString(R.string.soledad)))
        LibrosT.add(Libros_Card("La Iliada", R.drawable.lailiada, getString(R.string.iliada)))
        LibrosT.add(Libros_Card("Orgullo y Perjuicio ", R.drawable.orgullo, getString(R.string.orgullo)))
        LibrosT.add(Libros_Card("Don quijote de la mancha", R.drawable.donquijote, getString(R.string.quijote)))

        lista = findViewById(R.id.clasico)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG)
                    .show()



                if (index == 0) {

                    val nombres = "100 años de soledad"
                    val autor = "Autor:  MIguel de Cervantes"
                    val sinopsis = getString(R.string.anios_desc)
                    val foto = R.drawable.anios_soledad
                    val imagen = R.drawable.gabriel
                    val cali = 4.5F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }

                if (index == 1) {

                    val nombres = "La Iliada"
                    val autor = "Autor: Homero"
                    val sinopsis = getString(R.string.iliada_desc)
                    val foto = R.drawable.lailiada
                    val imagen = R.drawable.iliada_homero
                    val cali = 3.2F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "Orgullo y Perjuicio"
                    val autor = "Autor: Jane Austen"
                    val sinopsis = getString(R.string.oyp_desc)
                    val foto = R.drawable.orgulloyprejui
                    val imagen = R.drawable.perjuicio
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "Don Quijite de la Mancha"
                    val autor = "Autor: Miguel de Cervantes"
                    val sinopsis = getString(R.string.quijote_desc)
                    val foto = R.drawable.donquijote
                    val imagen = R.drawable.mancha
                    val cali = 3.5F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

            }
        })

        lista?.adapter = adaptador

    }
}
