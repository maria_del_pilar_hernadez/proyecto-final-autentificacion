package com.example.sinopsislibros

import android.app.Application

class SinopsisLibros:Application() {

    companion object{
        lateinit var pref: Pref
    }

    override fun onCreate() {
        super.onCreate()
        pref=Pref(applicationContext)
    }
}