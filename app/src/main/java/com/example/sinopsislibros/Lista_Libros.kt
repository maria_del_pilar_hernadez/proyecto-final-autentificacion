package com.example.sinopsislibros

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_informacion.*

class Lista_Libros: AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro_terror)

        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("IT", R.drawable.it, getString(R.string.it)))
        LibrosT.add(Libros_Card("El Resplandor", R.drawable.elresplandor, getString(R.string.resplandor)))
        LibrosT.add(Libros_Card("El pozo y el péndulo ", R.drawable.elpozoypendulo, getString(R.string.pozo)))
        LibrosT.add(Libros_Card("Misery", R.drawable.misery, getString(R.string.misery)))

        lista = findViewById(R.id.terror)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG).show()

                if (index == 0) {

                    val nombres = "IT"
                    val autor = "Autor: Stephen King"
                    val sinopsis = getString(R.string.it_desc)
                    val foto = R.drawable.it
                    val imagen = R.drawable.eso
                    val cali = 3.5F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }
                if (index == 1) {

                    val nombres = "El Resplandor"
                    val autor = "Autor: Stephen King"
                    val sinopsis = getString(R.string.resplandor_desc)
                    val foto = R.drawable.elresplandor
                    val imagen = R.drawable.resplando_dos
                    val cali = 2.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "El Pozo y el Péndulo"
                    val autor = "Autor: Edgar Allan Poe"
                    val sinopsis = getString(R.string.pozoyp)
                    val foto = R.drawable.elpozoypendulo
                    val imagen = R.drawable.pozoypendulo
                    val cali = 2.8F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "Misery"
                    val autor = "Autor: Stephen King"
                    val sinopsis = getString(R.string.misery_desc)
                    val foto = R.drawable.misery
                    val imagen = R.drawable.dos
                    val cali = 3.8F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

            }
        })

        lista?.adapter = adaptador

    }
}
