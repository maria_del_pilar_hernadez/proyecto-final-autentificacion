package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Libro_romance : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: AdaptadorLibros? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_libro_romance)
        val LibrosT = ArrayList<Libros_Card>()

        LibrosT.add(Libros_Card("Eleanor y Park", R.drawable.eleonorypark, getString(R.string.eleanor)))
        LibrosT.add(Libros_Card("A todos los chicos de los que me enamore", R.drawable.atodos, getString(R.string.chicos)))
        LibrosT.add(Libros_Card("Romeo y Julieta ", R.drawable.romeoyjulieta, getString(R.string.ryj)))
        LibrosT.add(Libros_Card("Maravilloso Desastre", R.drawable.desastre, getString(R.string.antes_de_ti)))

        lista = findViewById(R.id.romance)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorLibros(this, LibrosT, object : ClickListener {

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, LibrosT.get(index).nombre, Toast.LENGTH_LONG)
                    .show()

                if (index == 0) {

                    val nombres = "Eleanor and Park"
                    val autor = "Autor: Rainbow Rowell"
                    val sinopsis = getString(R.string.eleanor_desc)
                    val foto = R.drawable.eleonorypark
                    val imagen = R.drawable.eleonorypark_dos
                    val cali = 4.2F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)


                }

                if (index == 1) {

                    val nombres = "A todos los chicos de los que me enamore"
                    val autor = "Autor: Jenny Han"
                    val sinopsis = getString(R.string.chicos_desc)
                    val foto = R.drawable.atodos
                    val imagen = R.drawable.atodos_dos
                    val cali = 3.5F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)


                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 2) {

                    val nombres = "Romeo y Julieta"
                    val autor = "Autor: Willian Shakespeare"
                    val sinopsis = getString(R.string.romeo_desc)
                    val foto = R.drawable.romeoyjulieta
                    val imagen = R.drawable.romeoyjulieta_dos
                    val cali = 4.0F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

                if (index == 3) {

                    val nombres = "Maravilloso Desastre"
                    val autor = "Autor: Jaime McGuire"
                    val sinopsis = getString(R.string.desastre_desc)
                    val foto = R.drawable.maravillosodesastre
                    val imagen = R.drawable.maravillosodesastre_dos
                    val cali = 3.3F

                    val bundle = Bundle()
                    bundle.apply {
                        putString("key_nombre",nombres)
                        putString("key_autor",autor)
                        putString("key_sinopsis",sinopsis)
                        putInt("key_imagen", foto)
                        putInt("key_img", imagen)
                        putFloat("key_cal", cali)
                    }

                    val intent = Intent(applicationContext,Informacion::class.java).apply {
                        putExtras(bundle)
                    }
                    startActivity(intent)
                }

            }
        })

        lista?.adapter = adaptador

    }
}
