package com.example.sinopsislibros


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_registro.*


class Registro : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var nom:String
    private lateinit var apell:String
    private lateinit var correo:String
    private lateinit var con:String
    private lateinit var confirm:String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()
        dbReference = database.reference.child("User")
        registra.setOnClickListener() {

            registrarU()
        }
    }

    fun registrarU() {


        correo = correoo.text.toString()
        con = contraseña.text.toString()
        confirm = contraConfirm.text.toString()
        print("------------------------------------------------------")
        print("------------------------------------------------------")
        print("------------------------------------------------------")
        print(nom)
        print(apell)
        print(correo)
        print(con)
        print("------------------------------------------------------")
        print("------------------------------------------------------")
        print("------------------------------------------------------")

        if (correoo.text.isEmpty() || contraseña.text.isEmpty() || contraConfirm.text.isEmpty()){
            Toast.makeText(applicationContext, "Llene los campos", Toast.LENGTH_LONG).show()

        }
        else{
            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(correoo.text.toString(),
                    contraseña.text.toString()).addOnCompleteListener(this){
                    if (it.isSuccessful){
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                    }else{
                        Toast.makeText(applicationContext, "Llene los campos", Toast.LENGTH_LONG).show()
                    }
                }

        }
    }

    /*private fun registrarUsu(user: FirebaseUser?) {
        if (!TextUtils.isEmpty(user.toString())) {
            val nuevoUsuario: Usuario =
                Usuario(
                    nom,
                    apell,
                    correo,
                    con
                )
            val datos = hashMapOf(
                "Nombre" to "${nuevoUsuario.nombre}",
                "Apellidos" to "${nuevoUsuario.apellidos}",
                "Correo" to "${nuevoUsuario.correo}",
                "Clave" to "${nuevoUsuario.clave}"
            )


            dbReference.setValue(datos)


        } else {
            Toast.makeText(this, "Datos inconrrectos, intente de nuevo.", Toast.LENGTH_SHORT).show()
        }
    }*/

    private fun accion() {
        singOut()
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun singOut() {
        AuthUI.getInstance().signOut(this).addOnSuccessListener {
            finish()
        }.addOnFailureListener {
            Toast.makeText(this, "Ocurrio un error ${it.message}", Toast.LENGTH_LONG).show()
        }
    }

    private fun verificacionCorreo(user: FirebaseUser?) {
        user?.sendEmailVerification()?.addOnCompleteListener(this) { task ->
            if (task.isComplete) {
                Toast.makeText(this, "Correo enviado", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Correo no valido", Toast.LENGTH_LONG).show()
            }
        }
    }

}